# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_nums = []
  nums.each_with_index do | num, idx |
    break if nums[idx + 1].nil?
    next if nums[idx] + 1 == nums[idx + 1]
    difference = nums[idx + 1] - nums[idx]
    add = 1
    while add < difference
      missing_nums << nums[idx] + add
      add += 1
    end
  end

  missing_nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  binary_digits = binary.length - 1
  ten_sum = 0

  binary.split("").each do |digit|
    ten_sum += digit.to_i * (2 ** binary_digits)
    binary_digits -= 1
  end

  ten_sum
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    select_hash = {}

    self.each do | key, value |
      select_hash[key] = value if prc.call(key, value)
    end
    select_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged_hash = {}
    all_keys = (self.keys + hash.keys).uniq
    prc = Proc.new { |key, val1, val2| val2 } if prc.nil?
    all_keys.each do |k|
      if self[k] != nil && hash[k] != nil
        merged_hash[k] = prc.call(k, self[k], hash[k])
      elsif self[k].nil?
        merged_hash[k] = hash[k]
      else
        merged_hash[k] = self[k]
      end
    end
    merged_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas = {-1 => -1, 0 => 2, 1 => 1}
  return lucas[n] if lucas.key?(n)

  if n > 1
    idx = 2

    while idx <= n
      lucas[idx] = lucas[idx - 1] + lucas[idx - 2]
      idx += 1
    end

  else
    idx = -2
    while idx >= n
      lucas[idx] = (lucas[idx + 2]) - lucas[idx + 1]
      idx -= 1
    end

  end
  lucas[n]
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)

  def palindrome?(string)
    return true if string == string.reverse
    false
  end

  longest_string = ""
  longest_count = 0

  string.split("").each_with_index do |letter, idx|
    idx2 = idx
    while idx2 < string.length
      substring = string[idx..idx2]
      if palindrome?(substring) && substring.length > longest_count
        longest_string = substring
        longest_count = substring.length
      end
      idx2 += 1
    end
  end
  return false if longest_count <= 2
  longest_count
end
